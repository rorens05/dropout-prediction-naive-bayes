class AddFilterToDropOutPrediction < ActiveRecord::Migration[6.0]
  def change
    add_column :drop_out_predictions, :school_year, :string
    add_column :drop_out_predictions, :semister, :string
  end
end
