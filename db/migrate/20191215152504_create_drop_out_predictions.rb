class CreateDropOutPredictions < ActiveRecord::Migration[6.0]
  def change
    create_table :drop_out_predictions do |t|
      t.string :name
      t.string :year
      t.string :course
      t.string :location
      t.decimal :drop
      t.decimal :gwa
      t.string :gender
      t.string :action

      t.timestamps
    end
  end
end
