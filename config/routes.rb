Rails.application.routes.draw do
  resources :drop_out_predictions
  root to: 'home#index'
  get 'reports', :to => 'home#report'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
