class DropOutPredictionsController < ApplicationController
  before_action :set_drop_out_prediction, only: [:show, :edit, :update, :destroy]
  before_action :set_semister
  # GET /drop_out_predictions
  # GET /drop_out_predictions.json
  def index
    
    @drop_out_predictions = DropOutPrediction.all
    if (params["school_year"] != "all" && params["school_year"] != nil)
      @drop_out_predictions = @drop_out_predictions.where(school_year: params["school_year"])
    end

    if (params["semister"] != "all" && params["semister"] != nil)
      @drop_out_predictions = @drop_out_predictions.where(semister: params["semister"].split(" ").map(&:capitalize).join(" "))
    end
    @drop_out_predictions = @drop_out_predictions.order(:name)

  end

  # GET /drop_out_predictions/1
  # GET /drop_out_predictions/1.json
  def show
  end

  # GET /drop_out_predictions/new
  def new
    @drop_out_prediction = DropOutPrediction.new
  end

  # GET /drop_out_predictions/1/edit
  def edit
  end

  # POST /drop_out_predictions
  # POST /drop_out_predictions.json
  def create
    @drop_out_prediction = DropOutPrediction.new(drop_out_prediction_params)
    @drop_out_prediction.action = predictAction(@drop_out_prediction)
    respond_to do |format|
      if @drop_out_prediction.save
        format.html { redirect_to @drop_out_prediction, notice: 'Drop out prediction was successfully created.' }
        format.json { render :show, status: :created, location: @drop_out_prediction }
      else
        format.html { render :new }
        format.json { render json: @drop_out_prediction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /drop_out_predictions/1
  # PATCH/PUT /drop_out_predictions/1.json
  def update
    respond_to do |format|
      if @drop_out_prediction.update(drop_out_prediction_params)
        @drop_out_prediction.action = predictAction(@drop_out_prediction)
        @drop_out_prediction.save
        format.html { redirect_to @drop_out_prediction, notice: 'Drop out prediction was successfully updated.' }
        format.json { render :show, status: :ok, location: @drop_out_prediction }
      else
        format.html { render :edit }
        format.json { render json: @drop_out_prediction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /drop_out_predictions/1
  # DELETE /drop_out_predictions/1.json
  def destroy
    @drop_out_prediction.destroy
    respond_to do |format|
      format.html { redirect_to drop_out_predictions_url, notice: 'Drop out prediction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # set semister list
    def set_semister
      @semisters = []
      (2010..(Date.today().year)).each do |year|
        @semisters.push(year.to_s + "-" + (year + 1).to_s)
      end
      @semisters.reverse
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_drop_out_prediction
      @drop_out_prediction = DropOutPrediction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def drop_out_prediction_params
      params.require(:drop_out_prediction).permit(:name, :year, :course, :location, :drop, :gwa, :gender, :action, :school_year, :semister)
    end

    # predict the action of a student
    def predictAction(test)

      gwa     = "Very High"
      p_drop  = "Very High" 
      loc     = "Others"

      if (test.gwa == nil || test.drop == nil )
        return "error"
      end

      if(test.gwa < 75)
        gwa = "Fail"
      elsif (test.gwa < 80)
        gwa = "Low"
      elsif (test.gwa < 85)
        gwa = "Medium"
      elsif (test.gwa < 90)
        gwa = "High"
      else 
        gwa = "Very High"
      end

      if(test.drop <= 15)
        p_drop = "Very Low"
      elsif (test.drop <= 30)
        p_drop = "Low"
      elsif (test.drop <= 60)
        p_drop = "High"
      else 
        p_drop = "Very High"
      end

      if("Others" == test.location)
        loc = "Very Far"
      elsif ("Dagupan City" == test.location || "Mangaldan" == test.location || "Calasiao" == test.location || "Binmaley" == test.location)
        loc = "Near"
      else
        loc = "Far"
      end

      nbayes = YAML.load_file("model.yml")    
      result = nbayes.classify({location: loc, drop: p_drop, gwa: gwa, gender: test.gender})
      r_continue = result["Continue"]
      r_drop = result["Drop"]
      if r_drop > r_continue
        return "DROP"
      else
        return "CONTINUE"
      end
    end
end
