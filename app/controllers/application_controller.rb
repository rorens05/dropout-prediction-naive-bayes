class ApplicationController < ActionController::Base

  before_action :bypassIframeSecurity

  def bypassIframeSecurity

    response.headers.delete "X-Frame-Options"

  end
end
