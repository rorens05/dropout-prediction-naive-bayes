// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")
require("bootstrap/dist/js/bootstrap")

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)


document.addEventListener('turbolinks:load', () => { 
  let location = document.getElementById("location")
  console.log(location)
  if(location != null){
    let semister = document.getElementById("semister")
    let school_year = document.getElementById("school_year")
    
    let semister_value = semister.value
    let school_year_value = school_year.value
    
    let changeHandler = () => {
      semister_value = semister.value
      school_year_value = school_year.value
      window.location.href = `/${location.value}?semister=${encodeURI(semister_value)}&school_year=${school_year_value}`
    }
    semister.onchange = changeHandler
    school_year.onchange = changeHandler
  }

})