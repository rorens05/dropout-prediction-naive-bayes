json.extract! drop_out_prediction, :id, :name, :year, :course, :location, :drop, :gwa, :gender, :action, :created_at, :updated_at
json.url drop_out_prediction_url(drop_out_prediction, format: :json)
