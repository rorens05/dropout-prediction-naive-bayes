class DropOutPrediction < ApplicationRecord
  validates :name, presence: true
  validates :year, presence: true
  validates :course, presence: true
  validates :location, presence: true
  validates :drop, presence: true
  validates :gwa, presence: true
  validates :gender, presence: true
end
