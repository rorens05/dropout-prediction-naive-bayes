require 'test_helper'

class DropOutPredictionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @drop_out_prediction = drop_out_predictions(:one)
  end

  test "should get index" do
    get drop_out_predictions_url
    assert_response :success
  end

  test "should get new" do
    get new_drop_out_prediction_url
    assert_response :success
  end

  test "should create drop_out_prediction" do
    assert_difference('DropOutPrediction.count') do
      post drop_out_predictions_url, params: { drop_out_prediction: { action: @drop_out_prediction.action, course: @drop_out_prediction.course, drop: @drop_out_prediction.drop, gender: @drop_out_prediction.gender, gwa: @drop_out_prediction.gwa, location: @drop_out_prediction.location, name: @drop_out_prediction.name, year: @drop_out_prediction.year } }
    end

    assert_redirected_to drop_out_prediction_url(DropOutPrediction.last)
  end

  test "should show drop_out_prediction" do
    get drop_out_prediction_url(@drop_out_prediction)
    assert_response :success
  end

  test "should get edit" do
    get edit_drop_out_prediction_url(@drop_out_prediction)
    assert_response :success
  end

  test "should update drop_out_prediction" do
    patch drop_out_prediction_url(@drop_out_prediction), params: { drop_out_prediction: { action: @drop_out_prediction.action, course: @drop_out_prediction.course, drop: @drop_out_prediction.drop, gender: @drop_out_prediction.gender, gwa: @drop_out_prediction.gwa, location: @drop_out_prediction.location, name: @drop_out_prediction.name, year: @drop_out_prediction.year } }
    assert_redirected_to drop_out_prediction_url(@drop_out_prediction)
  end

  test "should destroy drop_out_prediction" do
    assert_difference('DropOutPrediction.count', -1) do
      delete drop_out_prediction_url(@drop_out_prediction)
    end

    assert_redirected_to drop_out_predictions_url
  end
end
