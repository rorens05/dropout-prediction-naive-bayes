require "application_system_test_case"

class DropOutPredictionsTest < ApplicationSystemTestCase
  setup do
    @drop_out_prediction = drop_out_predictions(:one)
  end

  test "visiting the index" do
    visit drop_out_predictions_url
    assert_selector "h1", text: "Drop Out Predictions"
  end

  test "creating a Drop out prediction" do
    visit drop_out_predictions_url
    click_on "New Drop Out Prediction"

    fill_in "Action", with: @drop_out_prediction.action
    fill_in "Course", with: @drop_out_prediction.course
    fill_in "Drop", with: @drop_out_prediction.drop
    fill_in "Gender", with: @drop_out_prediction.gender
    fill_in "Gwa", with: @drop_out_prediction.gwa
    fill_in "Location", with: @drop_out_prediction.location
    fill_in "Name", with: @drop_out_prediction.name
    fill_in "Year", with: @drop_out_prediction.year
    click_on "Create Drop out prediction"

    assert_text "Drop out prediction was successfully created"
    click_on "Back"
  end

  test "updating a Drop out prediction" do
    visit drop_out_predictions_url
    click_on "Edit", match: :first

    fill_in "Action", with: @drop_out_prediction.action
    fill_in "Course", with: @drop_out_prediction.course
    fill_in "Drop", with: @drop_out_prediction.drop
    fill_in "Gender", with: @drop_out_prediction.gender
    fill_in "Gwa", with: @drop_out_prediction.gwa
    fill_in "Location", with: @drop_out_prediction.location
    fill_in "Name", with: @drop_out_prediction.name
    fill_in "Year", with: @drop_out_prediction.year
    click_on "Update Drop out prediction"

    assert_text "Drop out prediction was successfully updated"
    click_on "Back"
  end

  test "destroying a Drop out prediction" do
    visit drop_out_predictions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Drop out prediction was successfully destroyed"
  end
end
